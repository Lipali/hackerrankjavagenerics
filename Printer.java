
import java.io.IOException;
import java.lang.reflect.Method;

class Printer
{
    <T> void printArray(T[] e){
        for (T element : e){
            System.out.println(element);
        }
    }

}

